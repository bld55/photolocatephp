-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 14 Février 2015 à 10:58
-- Version du serveur :  5.6.22
-- Version de PHP :  5.6.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `photolocate`
--

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id_img` int(11) NOT NULL,
  `label_img` varchar(140) NOT NULL,
  `lat_img` double NOT NULL,
  `lng_img` double NOT NULL,
  `id_ville` int(11) NOT NULL,
  `lien_img` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id_img`, `label_img`, `lat_img`, `lng_img`, `id_ville`, `lien_img`) VALUES
(1, 'Le Chat Noir', 48.686192, 6.171571, 1, 'img/chat_noir.jpg'),
(2, 'Brasserie Excelsior', 48.690652, 6.175511, 1, 'img/excelsior.jpg'),
(3, 'Fac de Lettres', 48.695456, 6.167465, 1, 'img/fac_lettres.jpg'),
(4, 'Gare de Nancy', 48.689889, 6.174862, 1, 'img/gare.jpg'),
(5, 'Opéra de Nancy', 48.693873, 6.183845, 1, 'img/opera.jpg'),
(6, 'Parc de la Pépinière', 48.697633, 6.185656, 1, 'img/pepiniere.jpg'),
(7, 'Place Stanislas', 48.693547, 6.183488, 1, 'img/place_stan.jpg'),
(8, 'Place Saint Epvre', 48.696293, 6.179288, 1, 'img/saint_epvre.jpg'),
(9, 'Rue Saint Jean', 48.689743, 6.179193, 1, 'img/saint_jean.jpg'),
(10, 'Galerie Saint Sébastien', 48.688833, 6.181558, 1, 'img/saint_seb.jpg'),
(12, 'Arc de Triomphe', 48.873792, 2.295028, 3, 'img/arc_de_triomphe.jpg'),
(13, 'Tour Eiffel', 48.85837, 2.294481, 3, 'img/tour_eiffel.jpg'),
(14, 'Moulin Rouge', 48.884123, 2.332252, 3, 'img/moulin_rouge.jpg'),
(15, 'Musée du Louvre', 48.860611, 2.337644, 3, 'img/musee_du_louvre.jpg'),
(16, 'Sacré-Coeur', 48.886705, 2.343104, 3, 'img/sacre_coeur.jpg'),
(17, 'Cathédrale Notre-Dame de Paris', 48.852968, 2.349902, 3, 'img/notre_dame.jpg'),
(18, 'Palais de l''Elysée', 48.870416, 2.316754, 3, 'img/elysee.jpg'),
(19, 'Musée Grévin', 48.871838, 2.34222, 3, 'img/musee_grevin.jpg'),
(20, 'Pont des Arts', 48.858342, 2.337508, 3, 'img/pont_des_arts.jpg'),
(21, 'Place Vendôme', 48.8668099, 2.3283038, 3, 'img/place_vendome.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE IF NOT EXISTS `partie` (
  `id_partie` int(11) NOT NULL,
  `points_partie` int(11) DEFAULT NULL,
  `pseudo_partie` varchar(30) NOT NULL,
  `token_partie` varchar(256) NOT NULL,
  `status_partie` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `partie`
--

INSERT INTO `partie` (`id_partie`, `points_partie`, `pseudo_partie`, `token_partie`, `status_partie`) VALUES
(1, 78, 'Thomas', '176872811854df1bbbeb9911.36801544', 'Terminée'),
(2, 98, 'Sébastien', '134107198354df1bd947e7e3.81566139', 'Terminée'),
(3, 52, 'Laura', '42761303554df1bf4ed0d23.24598617', 'Terminée'),
(4, 0, 'Charline', '40723104754df1c11c2a5e4.32403944', 'en cours');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL,
  `nom_user` varchar(50) NOT NULL,
  `login_user` varchar(30) NOT NULL,
  `pass_user` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id_user`, `nom_user`, `login_user`, `pass_user`) VALUES
(2, 'test', 'test', '$2y$12$5gi3XWWXSGLeWZDMkT54seG0AoF25sniHiq0ERMXD9U5mXAXMaTii');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `id_ville` int(11) NOT NULL,
  `label_ville` varchar(50) NOT NULL,
  `lat_ville` double NOT NULL,
  `lng_ville` double NOT NULL,
  `zoom_ville` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id_ville`, `label_ville`, `lat_ville`, `lng_ville`, `zoom_ville`) VALUES
(1, 'Nancy', 48.69325, 6.1800374, 13),
(3, 'Paris', 48.8587068, 2.3455434, 11);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id_img`), ADD KEY `id_img` (`id_img`), ADD KEY `id_img_2` (`id_img`), ADD KEY `id_ville` (`id_ville`), ADD KEY `id_img_3` (`id_img`), ADD KEY `label_img` (`label_img`), ADD KEY `lat_img` (`lat_img`), ADD KEY `lng_img` (`lng_img`), ADD KEY `id_ville_2` (`id_ville`);

--
-- Index pour la table `partie`
--
ALTER TABLE `partie`
  ADD PRIMARY KEY (`id_partie`), ADD KEY `id_user` (`pseudo_partie`), ADD KEY `id_score` (`id_partie`), ADD KEY `points_score` (`points_partie`), ADD KEY `id_user_2` (`pseudo_partie`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`), ADD UNIQUE KEY `id_user` (`id_user`), ADD KEY `id_user_2` (`id_user`), ADD KEY `nom_user` (`nom_user`), ADD KEY `login_user` (`login_user`), ADD KEY `pass_user` (`pass_user`);

--
-- Index pour la table `ville`
--
ALTER TABLE `ville`
  ADD PRIMARY KEY (`id_ville`), ADD KEY `id_ville` (`id_ville`), ADD KEY `label_ville` (`label_ville`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id_img` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `partie`
--
ALTER TABLE `partie`
  MODIFY `id_partie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `ville`
--
ALTER TABLE `ville`
  MODIFY `id_ville` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`id_ville`) REFERENCES `ville` (`id_ville`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
