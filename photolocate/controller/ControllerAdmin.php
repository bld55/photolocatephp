<?php
namespace photolocate\controller;

Class ControllerAdmin {

	public function actionAccueilAdmin(){
		if(!isset($_SESSION['user'])){
			
			//$erreur = 'Erreur de connexion';

			$a = array();
			$v = new \photolocate\view\VAccueilAdmin($a,$erreur=null);
			$v->display();

		}
		else{

			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin/espaceadmin');

		}
	}


	public function actionConnexionAdmin($login,$mdp){
			
		$conf = \photolocate\model\User::CheckConnection($login,$mdp);

		if($conf == true){

			

			$u = new \photolocate\model\User();
			$u->login=$login;
			$_SESSION['user']=$u;
			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin/espaceadmin');


		}else{

			
			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin');
			//echo "La connexion a échoué !" ;
		}
			//$v = new \photolocate\view\VAccueilAdmin($a);
			//$v->display();
		//}
	}

	public function actionEspaceAdmin(){
		if(isset($_SESSION['user'])){
			$a = $_SESSION['user'];
			$v = new \photolocate\view\VAdmin($a);
			$v->display();
		}
		else{

			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin');
			//echo "Vous devez être connecté pour voir cette page !";
		}
	}

	public function actionFormImage(){
		if(isset($_SESSION['user'])){

			$r = \photolocate\model\Ville::All();
			echo gettype($r);
			$v = new \photolocate\view\VAdminFormImage($r->toArray());
			$v->display();
		}
		else{

			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin');
			//echo "Vous devez être connecté pour voir cette page !";
		}
	}


	public function actionAddImage($label,$lat,$lgt,$id_ville,$nomFichier){
		$a = array();
		$a = [
			'label' => $label,
			'lat' => $lat,
			'lgt' => $lgt,
			'id_ville' => $id_ville,
			'img' => "img/".$nomFichier,];

		if(isset($_POST['save'])){
			$e = \photolocate\model\Image::add($a);
			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin/addimage');
		}
	}

	public function actionAdminDeconnexion(){
		
		$a = array();
		$v = new \photolocate\view\VDeconnexion($a);
		$v->display();
		session_unset();
		session_destroy();
		\Slim\Slim::getInstance()->redirect('/photolocatephp/admin');
		
	}

	public function actionFormNewUser(){
	

		if(isset($_SESSION['user'])){
		
			$a = array();
			$v = new \photolocate\view\VFormNewUser($a);
			$v->display();
		}
		else{

			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin');
			//echo "Vous devez être connecté pour voir cette page !";
		}
	
	}


	public function actionAddUser($nom,$login,$mdp){

		
		$a = [
		'nom' => $nom,
		'login' => $login,
		'mdp' => $mdp,];

		if(isset($mdp)){
			echo 'coucou';
			$e = \photolocate\model\User::add($a);
			\Slim\Slim::getInstance()->redirect('/photolocatephp/admin/espaceadmin');
			
		}

	}

}
