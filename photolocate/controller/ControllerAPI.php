<?php
namespace photolocate\controller;

Class ControllerAPI{

	public function actionAPIStartGame(){
		// Verifier si $session token existe ou non pour eviter la triche
		
		//création d'1 token 
		// inscrit nouvelle entrée dans la bdd
		// du coup l'id est renvoyée
		$_SESSION['token']= uniqid(rand(), true);
		
		$a=[
			"pseudo_partie" => $_POST['pseudo'],
			"points_partie" => 0,
			"token_partie" => $_SESSION['token'],
		];

		$partie = \photolocate\model\Partie::add($a);

		$infos_ville = \photolocate\model\Ville::findById($_POST['ville']);
		$_SESSION['id_ville'] = $infos_ville[0]->id_ville;

		//$_SESSION['partie'] = $partie;
		$infos_partie=[
			"id_partie" => $partie->id_partie,
			"token_partie" => $partie->token_partie,
			"lat_ville" => $infos_ville[0]->lat_ville,
			"lng_ville" => $infos_ville[0]->lng_ville,
			"id_ville" => $infos_ville[0]->id_ville,
			"zoom_ville" => $infos_ville[0]->zoom_ville,
		];


		echo json_encode($infos_partie);
	}

	public function actionAPIGetInfoPartie($token, $id_ville){
		if(isset($_SESSION['token'])){
			//echo "token ok";
			if($_SESSION['token']==$token){

				//$ville = \photolocate\model\Ville::findById($id_ville);
				$nbphoto = \photolocate\model\Image::countImages($id_ville);

				$infos=[
					//"id_ville" => $ville[0]->id_ville,
					//"nom_ville" => $ville[0]->label_ville,
					//"lat" => $ville[0]->lat_ville,
					//"lng" => $ville[0]->lng_ville,
					"nbphoto" => $nbphoto,
				];

				//$_SESSION['infos'] = $infos;

				echo json_encode($infos);

			}else{
				echo "session ok mais token correspond pas";

			}
		}else{
			echo "pas de session token";
		}
	}

	public function actionAPIGetInfoPhotos($token, $id_ville){


		//récupère tout depuis la bdd
		//if(isset($_SESSION['token'])){
		//	if($_SESSION['token']==$token){
				$images = \photolocate\model\Image::findByIdVille($id_ville);
				$photos=[] ;
				foreach($images as $image){
					$photo = array();
					$photo['id_photo'] = $image->id_img;
					$photo['label_photo'] = $image->label_img;
					$photo['lat_photo'] = $image->lat_img;
					$photo['lng_photo'] = $image->lng_img;
					$photo['lien_photo'] = $image->lien_img;
					$photos[] = $photo;
				}

				echo json_encode($photos);
				//echo ("allo");
		//	}
		//}

	}
	public function actionAPIPutInfoPartie($id, $token, $score){
		//if(isset($_SESSION['token'])){
		//	if($_SESSION['token']==$token){
				$update = \photolocate\model\Partie::updatePartie($id, $token, $score);
		//	}
		//}

	}

	public function actionAPIGetHighscore(){
		$highscores = \photolocate\model\Partie::getHighscore();
		$hs = [];
		$i = 1;
		foreach($highscores as $highscore){
			if($highscore->points_partie>0){
				$tab = array();
				$tab['rank'] = $i;
				$tab['pseudo'] = $highscore->pseudo_partie;
				$tab['score'] = $highscore->points_partie;
				$hs[] = $tab;
				$i++;
			}
		}

		echo json_encode($hs);
	}
}