<?php
namespace photolocate\controller;

Class Controller{
	
	public function actionPlayGame(){
		
		$app= \Slim\Slim::getInstance();
		$url = $app->urlFor('playgames', array());
		$villes = \photolocate\model\Ville::All();

		$array = [
			'url' => $url,
			'villes' => $villes->toArray(),
		];

		$v = new \photolocate\view\VPlayGames($array);
		$v->display();
	}

	public function actionStartGame(){
		
		$app= \Slim\Slim::getInstance();
		$array= [
			'pseudo' => $app->request->post('pseudo'),
			'ville' => $app->request->post('ville'),
		];

		$v = new \photolocate\view\VStartGame($array);
		$v->display();
	}

}
