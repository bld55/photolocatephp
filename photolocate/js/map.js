var map;
var marker;

// Verification de l'existence du champ hidden pour le pseudo
if ($("#hpseudo").length ) { // s'il existe, execution des appels api
	var ville = $('#hville').attr('value');
    var pseudo = $('#hpseudo').attr('value');
    postPlayerInfos(pseudo, ville);
}

// initialisation de la carte suivant les parametres lattitude et longitude
function initMap(lat,lng, zoom){
	map = L.map('map', { zoomControl:false }).setView([lat, lng], zoom);
	//Pour eviter de tricher !
	map.dragging.disable();
	map.touchZoom.disable();
	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();

	// apparence de la map
	L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png').addTo(map);

}

// Affichage du temps dans une div
var divComp = $('#compteur');

// appel au post ajax avec le pseudo renseigné
//renvoie en JSON 1 id et 1 token
function postPlayerInfos(pseudo, ville){
	//appel ajax
	$.post('/photolocatephp/play/games', {pseudo: pseudo, ville: ville}).done(function(msg){
		var donnees = JSON.parse(msg);
		var id = donnees.id_partie;
		var token = donnees.token_partie;

		var lat = donnees.lat_ville;
		var lng = donnees.lng_ville;
		var zoom = donnees.zoom_ville;
		var id_ville = donnees.id_ville;

		console.log(donnees);

		getInfosGenerales(id, token, lat, lng, zoom);
	})
	.fail( function(xhr, textStatus, errorThrown) {
        alert(xhr.responseText);
    });
}

// appel api get pour récupérer les informations sur lattitude longitude de la ville selectionnée
function getInfosGenerales(id, token, lat, lng, zoom){
	var url = "/photolocatephp/play/games/"+id+"?token="+token;
	$.get(url).done(function(msg){
		var donnees = JSON.parse(msg);

		var nbphoto = donnees.nbphoto;

		// initialisation de la carte
		initMap(lat, lng, zoom);

		getInfosPhotos(token,id);
	}).fail( function(xhr, textStatus, errorThrown) {
        alert(xhr.responseText);
    });
}

// appel api get pour récupérer les données des photos (lat, lng, label, etc)
function getInfosPhotos(token, id_partie){
	var url = "/photolocatephp/play/games/"+id_partie+"/photos/?token="+token;
	$.get(url).done(function(msg){
		var donnees = JSON.parse(msg);

		//variables pour parcours donnees
		var compteur = 0;
		var score = 0;
		var counter = 0;
		var scoreTotal = 0;

		/*    AFFICHAGE PREMIERE PHOTO    */
		var style = 'style="max-height=150px;"';
		if(compteur<=1){
			counter = 0;
			interval = setInterval(function(){
				++counter;
				divComp.html("<b>"+counter+'</b>');
			}, 1000);
			var style = 'style="height:200px;"'
			$('#photos').html('<img id="images" src="'+donnees[compteur].lien_photo+'" '+style+'/>');
		}


		// event on click sur la carte
		map.on('click', function(e){
			// 'reset' le timer
			clearInterval(interval);

			/* 	  CALCUL DISTANCE   */
			// création d'un latlng avec compteur et donnees
			var point = getPoint(compteur, donnees);
			// calcul distance entre le curseur et le point réponse
			var d = point.distanceTo(e.latlng);

			/*    CALCUL TEMPS    */
			// affiche temps dans une div spéciale, update à chaque seconde
			interval = setInterval(function(){
				++counter;
				divComp.html("<b>"+counter+'</b>');
			}, 1000);


			/*    CALCUL SCORE    */
			// distance en mètres
			if(d < 500){
				score = 3;
		   	}else{
		   		if(d < 1000){
		   			score = 2;
		   		}else{
		   			score = 1;
		   		}	
			}
			
			// bonus pour le temps mis
			var bonus;
			var scoreDeBase = score; //Pour l'affichage en bas

			if(counter < 2){
				bonus = "x4";
				score = score * 4;
			}else{
				if(counter < 5){
					bonus = "x2";
					score = score * 2;
				}else{
					bonus = "x1";
				}
			}
			// calcul du score total + affichage
			scoreTotal = scoreTotal + score;
			$("#scoreImage").html(score+" ("+scoreDeBase+" + bonus "+bonus+"!)");
			$("#scoreTotal").html(scoreTotal);

			// reset du score à 0 pour la prochaine image
			score = 0;

			/*    AFFICHAGE DE TOUT    */
			
			// incrémentation du compteur
			++compteur;

			// Si la dernière image est apparue et que la dernière réponse est donnée
			if (compteur === 10){
				// popup
				$( "#dialog-confirm" ).dialog({
				  	resizable: false,
				  	height:350,
				  	width:500,
				  	modal: true,
				  	open: function() {
				  		getHighscore();
				    	var texteScore = 'Votre score est de <b>'+scoreTotal+'</b>.</br>';
				      	$(this).html(texteScore);
				    },
				  	buttons: {
				    	"Sauvegarder le score": function() {
				      		putInfos(id_partie, token, scoreTotal);
				    	},
				    	"Annuler": function() {
				      		window.location.replace("/photolocatephp/play");
				    	}
				
					}
				});
			}

			// affichages photos (hors première)
			var style = 'style="height:200px;"';
			$('#photos').html('<img id="images" src="'+donnees[compteur].lien_photo+'" '+style+'/>');
			// reset du timer pour la prochaine image
			counter = 0;
		});
	})
	.fail( function(xhr, textStatus, errorThrown) {
        alert(xhr.responseText);
    });
}



function getPoint(compteur, donnees){
	// retourne un LatLng plutot qu'un point/marker (comparaison avec l'event + simple)
	return new L.LatLng(donnees[compteur].lat_photo, donnees[compteur].lng_photo);
}

// appel api PUT pour update/sauvegarder la partie
function putInfos(id_partie, token, score){
	var url = "/photolocatephp/play/games/"+id_partie+"?token="+token;
	$.ajax({
		  url: url,
		  type: 'PUT',
		  data: {id_partie: id_partie, token: token, score: score},
		  success: function(data) {
		  	window.location.replace("/photolocatephp/play");

		  }
	});
}

// Affiche les 5 meilleurs scores - HORS SCORE COURANT :(
function getHighscore(){
	$.get('/photolocatephp/play/games/highscore/').done(function(msg){
		//console.log(msg);
		var donnees = JSON.parse(msg);
		var result = "";
		// vérifie s'il existe des scores valides
		if(donnees.length<1){
			result+="<hr/><div style='padding-left:2%;'><b>Il n'existe pas de score enregistré !</b></div>";
		}else{
			// parcours les resultats de la requete et affiche pour chacun les données nécessaires
			result += "<hr/><div style='padding-left:2%;'><b>Liste des meilleurs scores :</b><br/>";
			for (var i=0; i<donnees.length; i++){
				// c'est juste pour que ce soit plus attrayant ! Ne sert à rien.
				if(i==0){
					result += "<img src='img/icones/gold.png' style='height:25px;'/>";
				}else if(i==1){
					result += "<img src='img/icones/silver.png' style='height:25px;'/>";
				}else if(i==2){
					result += "<img src='img/icones/bronze.png' style='height:25px;'/>";
				}else{
					result += " <b style='padding-left:5px;'>"+donnees[i].rank+" </b>";
				}

				result += "- "+donnees[i].pseudo+" - "+donnees[i].score+" pts<br/>";
			}
			result += "</div>";
		}
		$("#dialog-confirm").append(result);
	});
}