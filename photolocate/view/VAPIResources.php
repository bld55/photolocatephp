<?php

namespace photolocate\view;

class VAPIResources extends \photolocate\view\View {

	public function __construct(Array $a) {
		parent::__construct($a);
		$this->layout = 'resources.html.twig';
		$this->arrayVar['a'] = $a;
	}

}