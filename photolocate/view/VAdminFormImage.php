<?php 
namespace photolocate\view;

class VAdminFormImage extends View {

	public function __construct($r) {
		
		parent::__construct($r);
		$this->layout = 'uploadadmin.html.twig';
		$this->arrayVar['r'] = $r;
	}
}