<?php 
namespace photolocate\view;

class VAccueilAdmin extends View {

	public function __construct($a, $erreur=null) {
		
		parent::__construct($a);
		$this->layout = 'accueiladmin.html.twig';
		
		$this->arrayVar['a'] = $a;
		if (!is_null($erreur)){
			$this->arrayVar['erreur'] = $erreur;
		}
		//$this->arrayVar['url'] = Slim::getInstance()->urlFor('commentaire',array($m->id));
	}
}