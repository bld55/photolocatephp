<?php

namespace photolocate\view;

class VStartGame extends \photolocate\view\View {

	public function __construct(Array $a=null) {
		parent::__construct($a);
		$this->layout = 'startgame.html.twig';
		if(isset($a)){
			$this->arrayVar['a'] = $a;
		}
		
	}

}