<?php

namespace photolocate\view;

class VPlayGames extends \photolocate\view\View {

	public function __construct(Array $a) {
		parent::__construct($a);
		$this->layout = 'playgames.html.twig';
		$this->arrayVar['a'] = $a;
	}

}