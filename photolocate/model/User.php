<?php
namespace photolocate\model;

Class User extends \Illuminate\Database\Eloquent\Model{

	protected $table='user';
	protected $primaryKey= 'id';
	public $timestamps=false;

	function __construct(){

	}

	public function scores(){
		return $this->HasMany('Score', 'id_user');
	}

	public static function findById($id){
		return User::where('id', '=', $id)->get();
	}

	public static function CheckConnection($login, $mdp){

    $u=User::where('login_user', '=', $login)
        ->get();

		if (password_verify($mdp, $u[0]->attributes['pass_user'])) {
      return true;
    }else{
      return false;
    }   
	}

	public static function add($array){

		$user = new User;

		$user->nom_user = $array['nom'];
		$user->login_user = $array['login'];
		$user->pass_user = $array['mdp'];
		//$user->salt_user = $array['salt_user'];

		$user->save();
	}

}

?>