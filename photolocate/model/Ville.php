<?php

namespace photolocate\model;

Class Ville extends \Illuminate\Database\Eloquent\Model{

	protected $table='ville';
	protected $primaryKey= 'id_ville';
	public $timestamps=false;

	function __construct(){

	}

	public function images(){
		return $this->HasMany('Image', 'id_ville');
	}

	public static function findById($id){
		return Ville::where('id_ville', '=', $id)->get();
	}

	public static function add($array){

		$ville = new Ville;

		$ville->label_ville = $array['label_ville'];
		$ville->lat_ville = $array['lat_ville'];
		$ville->lng_ville = $array['lng_ville'];


		$ville->save();
	}

}

?>