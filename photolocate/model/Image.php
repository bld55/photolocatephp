<?php

namespace photolocate\model;

Class Image extends \Illuminate\Database\Eloquent\Model{

	protected $table='image';
	protected $primaryKey= 'id';
	public $timestamps=false;

	function __construct(){

	}

	public function villes(){
		return $this->belongsTo('Ville','id_ville');
	}

	public static function findById($id){
		return Image::where('id_img', '=', $id)->get();
	}

	public static function add($a){

		$image = new Image;

		$image->label_img = $a['label'];
		$image->lat_img = $a['lat'];
		$image->lng_img = $a['lgt'];
		$image->id_ville = $a['id_ville'];
		$image->lien_img = $a['img'];

		$image->save();
	}
	public static function countImages($id_ville){
		return Image::where('id_ville', '=', $id_ville)->count();
	} 

	public static function findByIdVille($id_ville){
		return Image::where('id_ville', '=', $id_ville)->get();
	} 

}

?>