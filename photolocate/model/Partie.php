<?php

namespace photolocate\model;

Class Partie extends \Illuminate\Database\Eloquent\Model{

	protected $table='partie';
	protected $primaryKey= 'id_partie';
	public $timestamps=false;

	function __construct(){

	}

	public static function findById($id){
		return Partie::where('id_partie', '=', $id)->get()->first();
	}

	public static function add($a){

		$partie = new Partie;

		$partie->points_partie = $a['points_partie'];
		$partie->pseudo_partie = $a['pseudo_partie'];
		$partie->token_partie = $a['token_partie'];
		$partie->status_partie = 'en cours';
		
		$partie->save();
		return $partie;
	}

	public static function updatePartie($id, $token, $score){
		
		$partie = Partie::where('id_partie', '=', $id)->get()->first();

		$partie->points_partie = $score;
		$partie->status_partie = "Terminée";

		$partie->save();
	}

	public static function getHighscore(){
		return Partie::orderBy('points_partie', 'desc')->take(5)->get();
		//var_dump($scores);
	}

}

?>