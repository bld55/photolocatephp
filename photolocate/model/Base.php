<?php

namespace photolocate\model;

use Illuminate\Database\Capsule\Manager as Capsule;

class Base{		

	public static function EloConfig(){

		$config = parse_ini_file("config.ini");
		$capsule = new Capsule();		
		$capsule->addConnection(array(
		    'driver'    => 'mysql',
		    'host'      => $config['hostname'],
		    'database'  => $config['dbname'],
		    'username'  => $config['user'],
		    'password'  => $config['password'],
		    'charset'   => 'utf8',
		    'collation' => 'utf8_unicode_ci',
		    'prefix'    => ''
		));

	// Make this Capsule instance available globally via static methods... (optional)
	$capsule->setAsGlobal();

	// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
	$capsule->bootEloquent();

	}
}