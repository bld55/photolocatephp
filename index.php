<?php
require 'vendor/autoload.php';
global $app;
session_start();

$app = new \Slim\Slim();
\photolocate\model\Base::EloConfig();

$app->get('/',function() use($app){
	$app->redirect('play');
});

$app->get('/admin',function(){
	
	$AnnContr = new \photolocate\controller\ControllerAdmin();
	$AnnContr->actionAccueilAdmin();
});

$app->post('/admin',function() use($app){

	$login = $app->request->post('pseudo');
	$mdp = $app->request->post('mdp');
	$AnnContr = new \photolocate\controller\ControllerAdmin();
  	$AnnContr->actionConnexionAdmin($login,$mdp);
	

});

$app->get('/admin/espaceadmin', function(){
	$AnnContr = new \photolocate\controller\ControllerAdmin();
	$AnnContr->actionEspaceAdmin();
});

$app->get('/admin/addimage', function(){
	$AnnContr = new \photolocate\controller\ControllerAdmin();
	$AnnContr->actionFormImage();
});

$app->post('/admin/addimage',function() use($app){

	require 'fonctions_fichier.php';
	$label = $app->request->post('label');
	$lat = $app->request->post('lat');
	$lgt = $app->request->post('lgt');
	$id_ville = $app->request->post('liste_ville');
	if ($_FILES['img']['size'] != 0){
        $nomFichier = envoieFichier($_FILES['img']);
    }
	$AnnContr = new \photolocate\controller\ControllerAdmin();
  	$AnnContr->actionAddImage($label,$lat,$lgt,$id_ville,$nomFichier);

});



$app->get('/admin/deconnexion',function(){
	
	$AnnContr = new \photolocate\controller\ControllerAdmin();
	$AnnContr->actionAdminDeconnexion();
});




$app->get('/admin/newuser',function(){
	
	$AnnContr = new \photolocate\controller\ControllerAdmin();
	$AnnContr->actionFormNewUser();
});


$app->post('/admin/newuser',function() use($app){

	$nom = $app->request->post('nom');
	$login = $app->request->post('login');
	$mdp = password_hash($app->request->post('mdp'), PASSWORD_BCRYPT);

	$AnnContr = new \photolocate\controller\ControllerAdmin();
  	$AnnContr->actionAddUser($nom,$login,$mdp);
	
});


/*
*          *
*    JEU   *
*          *
*/
$app->get('/play', function(){

	$contr = new \photolocate\controller\Controller();
	$contr->actionPlayGame();
});

$app->post('/play',function(){

	$contr = new \photolocate\controller\Controller();
	$contr->actionStartGame();
	
})->name('playgames');;


/*
*          *
* API REST *
*          *
*/


$app->post('/play/games', function() use ($app){

	$contr = new \photolocate\controller\ControllerAPI();
	$contr->actionAPIStartGame();

});

$app->get('/play/games/:id',function($id) use ($app){
	$req= $app->request();
	$token= $req->params('token');
	//$id_ville = 1;//a modifier
	$contr = new \photolocate\controller\ControllerAPI();
	$id_ville = $_SESSION['id_ville'];
	$contr->actionAPIGetInfoPartie($token, $id_ville);
	//$contr->actionAPIGetInfoPartie($token);

});

$app->get('/play/games/:id/photos/',function($id) use ($app){
	$req= $app->request();
	$token= $req->params('token');
	$id_ville = $_SESSION['id_ville'];
	//var_dump($token);

	$contr = new \photolocate\controller\ControllerAPI();
	$contr->actionAPIGetInfoPhotos($token, $id_ville);
	//echo "GET token = ".$token;
});

$app->put('/play/games/:id',function($id) use ($app) {
	$req= $app->request();
	$token= $req->params('token');
    $score = $app->request->put('score');
	$contr = new \photolocate\controller\ControllerAPI();
	$contr->actionAPIPutInfoPartie($id,$token,$score);

});

$app->put('/play/games/:id',function($id) use ($app) {
	$req= $app->request();
	$token= $req->params('token');
    $score = $app->request->put('score');
	$contr = new \photolocate\controller\ControllerAPI();
	$contr->actionAPIPutInfoPartie($id,$token,$score);

});

$app->get('/play/games/highscore/', function(){
	$contr = new \photolocate\controller\ControllerAPI();
	$contr->actionAPIGetHighscore();
});

$app->run();